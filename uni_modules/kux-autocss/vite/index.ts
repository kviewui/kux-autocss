// plugin.ts
// import type { UserConfig } from 'vite';
import { PluginOptions } from '../src/types'
import { genInitConfig, initApp, initCssDir } from '../src/build/initConfig'
import { setConfig, readConfigFile, getConfig } from '../src/config'
import { EXT_NAME } from '../src/constant'
import { getExtName } from '../src/tools/utils'
import { hotReload } from '../src/tools/hotReloadFn'

const fs = require('fs');
const fs2 = fs.promises;
const micromatch = require('micromatch');

async function genConfig (config, options) {
	let newConfig;
	await genInitConfig(config, options)
	await initCssDir(config, options)
	// await initApp(config, options)
	newConfig = Object.assign(config, setConfig(readConfigFile(config, options)))
	
	return newConfig
}

export default function Autocss(options?: PluginOptions) {
	let globalConfig: any = null
	let viteConfig: any;
	
	return {
		name: 'vite-plugin-autocss',
		enforce: 'pre',
		async config (config: any) {
			viteConfig = config
			// await genInitConfig(config, options)
			// await initCssDir(config, options)
			// await initApp(config, options)
			// globalConfig = Object.assign(config, setConfig(readConfigFile(config, options)))
			globalConfig = await genConfig(config, options)
			
			return config
		},
		async transform(code, id) {
			const matchLetter : RegExpMatchArray | null = code.match(/(?<=\.)([\w\d]+)$/);
			if (matchLetter && !getConfig(EXT_NAME).includes(matchLetter[0])) {
				console.log('not match');
				return undefined;
			}
		
			/**
			 * 过滤文件
			 */
			const path = require('path');
			
			// 追加根目录到 includePatterns
			const root = process.env.UNI_INPUT_DIR || process.env.UNI_CLI_CONTEXT
			let includePaths = globalConfig?.include?.map(pattern => path.posix.join(root, pattern)) || [`${root}/pages/**`];
			if (!includePaths.length) {
				includePaths = [`${root}/pages/**`];
			}
			const excludePaths = globalConfig?.exclude?.map(pattern => path.posix.join(root, pattern));
			
			if (micromatch.isMatch(id, excludePaths)) {
				return null
			}
			
			let newCode = code;
			if (micromatch.isMatch(id, includePaths)) {
				const extName = getExtName(id)
				if (globalConfig?.extName.includes(`${extName}`) && (id.indexOf('type=style') < 0 && id.indexOf('type=script') < 0)) {
					if (globalConfig?.debug) {
						console.log('监听到目标文件修改：', id);
					}
					const { sourceCode } = await hotReload(code, extName, options);
					newCode = sourceCode
				}
			}
			
			return {
				code: newCode,
				map: null
			}
		}
	} as any;
}
