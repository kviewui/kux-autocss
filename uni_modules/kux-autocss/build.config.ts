import { defineBuildConfig } from 'unbuild'

export default defineBuildConfig({
	entries: [
		'./src/vite'
	],
	clean: true,
	declaration: true,
	failOnWarn: false,
	sourcemap: false
})