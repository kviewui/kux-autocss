/**
 * backgroundClip 规则
 */
import { BACKGROUND_CLIP_NAME_STR, BACKGROUND_CLIP_STR } from '../constant'

export default {
	regExp: new RegExp(`^(?<important1>!?)?${BACKGROUND_CLIP_NAME_STR}-(?<value>${BACKGROUND_CLIP_STR})?(?<important2>!?)$`),
	render: function ({ groups }) {
		const { value } = groups
		
		return {
			name: 'backgroundClip',
			css: [`background-clip: ${value}-box`]
		}
	}
}