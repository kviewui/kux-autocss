/**
 * text-align-last 规则
 */
export default {
    regExp: /^!?(text-align-last|text-last)-(?<value>auto|left|right|center|justify|start|end|initial|inherit)!?$/,
    render({ groups }) {
        const { value } = groups
        return {
            name: 'textAlignLast',
            css: [`text-align-last: ${value}`]
        }
    }
}
