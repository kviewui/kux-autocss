import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant'

export default {
    className: 'square',
    regExp: new RegExp(`^!?square-(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?!?$`),
    render({ groups }) {
        const { num, unit } = groups
        return { name: 'square', num, css: [`width: ${num}${unit}`, `height: ${num}${unit}`] }
    }
}
