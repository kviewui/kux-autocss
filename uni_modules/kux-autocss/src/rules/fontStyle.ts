/**
 * font-style 规则
 */
export default {
	regExp: new RegExp(`^!?(font-|)?(?<value>italic|not-italic)!?$`),
	render({ groups }) {
		let { value } = groups
		
		if (value === 'not-italic') {
			value = 'normal'
		}
		
		return {
			name: 'fontStyle',
			css: [`font-style: ${value}`]
		}
	}
}