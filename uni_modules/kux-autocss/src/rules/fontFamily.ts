/**
 * font-family 规则
 */
export default {
	regExp: /^!?font-(?<value>sans|serif|mono|cursive|fantasy|monospace|sans-serif)!?$/,
	render({ groups }) {
		const { value = 'sans', extName } = groups
		
		let fontFamily = ''
		
		if (value === 'mono') {
			fontFamily = `ui-monospace, SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace`
		} else if (value === 'serif') {
			fontFamily = `ui-serif, Georgia, Cambria, "Times New Roman", Times, serif`
			if (extName === 'uvue') {
				fontFamily = value
			}
		} else if (value === 'sans') {
			fontFamily = `ui-sans-serif, system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, "Noto Sans", sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji"`
		} else {
			fontFamily = value
		}
		
		return {
			name: 'fontFamily',
			css: [`font-family: ${fontFamily}`]
		}
	}
}