/**
 * display 规则
 */
import { DISPLAY_STR } from '../constant';

export default {
    // regExp: new RegExp(`^((display|d)-(?<value>${DISPLAY_STR}))`),
	regExp: new RegExp(`^!?(?<displayType1>${DISPLAY_STR})(?![^!])|!?((display|d)-(?<displayType2>${DISPLAY_STR}))!?$`),
    render: function ({ groups }) {
        let { displayType1, displayType2 } = groups;
		let value = displayType1 ?? displayType2
		if (value == 'hidden') {
			value = 'none'
		}
        return {
            name: 'display',
            css: [`display: ${value}`]
        }
    }
}
