/**
 * 圆角
 */
export default {
    regExp: /^circle$/,
    render: function () {
        return {
            name: 'circle',
            css: ['border-radius: 50%']
        }
    }
}
