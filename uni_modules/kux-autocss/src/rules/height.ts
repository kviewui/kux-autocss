/**
 * height 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant';

export default {
    regExp: new RegExp(`^!?(h|height)-(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?!?$`),
    render: function ({ groups }) {
        const { num = 0, unit } = groups;
        return {
            name: 'height',
            css: [`height: ${num}${unit}`]
        }
    }
}
