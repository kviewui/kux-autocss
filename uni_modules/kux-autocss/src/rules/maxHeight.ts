/**
 * max-height 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant'

export default {
    regExp: new RegExp(`!?^(max-h|max-height)-(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?!?$`),
    render({ groups }) {
        const { num, unit } = groups
        return { name: 'max-height', num, css: [`max-height: ${num}${unit}`] }
    }
}
