/**
 * flex 规则
 */
export default {
    regExp: /^!?(flex-|)?(?<type>shrink|grow)-(?<value>(0|[1-9]\d*)|initial|inherit)!?$/,
    render: function ({ groups }) {
        const { type, value} = groups;
        return {
            name: type === 'shrink' ? 'flexShrink' : 'flexGrow',
            css: [`flex-${type}: ${value}`]
        }
    }
}
