/**
 * border-radius 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR, RADIUS_DIRECTION_MAP } from '../constant';

export default {
    regExp: new RegExp(`^(?<important1>!?)?(border-radius|br|rounded)-((?<direction>[trbl]|tl|tr|bl|br)-)?(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?(?<important2>!?)?$`),
    render: function ({ groups }) {
        const { num, unit, direction } = groups;
		let css: string[] = []
		
		if (direction) {
			css = RADIUS_DIRECTION_MAP.get(direction).reduce((t: any, c: any) => [...t, `border-${c}-radius: ${num}${unit}`], [])
		} else {
			css = [`border-radius: ${num}${unit}`]
		}
        return {
            name: 'borderRadius',
            num,
            css: css
        }
    }
}
