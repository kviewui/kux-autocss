/**
 * border 规则
 */
import { UNIT_STR, DIRECTION_MAP, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant';

const getCss = (direction: any, num: any, unit: any) => {
    return DIRECTION_MAP
        .get(direction)
        .reduce((t: any, c: any) => {
            if (c) {
                // return [...t, `border-${c}-width: ${num}${unit}`, `border-${c}-style: solid`, `border-${c}-color: rgba(0,0,0,1)`];
				return [...t, `border-${c}-width: ${num}${unit}`]
            } else {
                // return [...t, `border-width: ${num}${unit}`, `border-style: solid`, `border-color: rgba(0,0,0,1)`];
				return [...t, `border-width: ${num}${unit}`]
            }
        }, [])
}

export default {
    regExp: new RegExp(`^(?<important1>!?)?(border|border-width|border-w)-((?<direction>[trblxy])-)?(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?(?<important2>!?)?$`),
    render({ groups }) {
        const { direction, num, unit } = groups;
        return {
            name: direction ? `border-${direction}` : 'border',
            num,
            css: getCss(direction, num, unit)
        }
    },
}
