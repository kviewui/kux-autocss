/**
 * pointer-events 规则
 */
export default {
	regExp: new RegExp(`^!?(pointer-events|pe)-(?<value>auto|none)!?$`),
	render({ groups }) {
		const { value } = groups
		
		return {
			name: 'pointerEvents',
			css: [`pointer-events: ${value}`]
		}
	}
}