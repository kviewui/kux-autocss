/**
 * flex-wrap 规则
 */
export default {
    regExp: /^!?(flex-wrap|flex)-(?<value>inherit|initial|nowrap|wrap|wrap-reverse)!?$/,
    render: function ({ groups }) {
        const { value } = groups;
        return {
            name: 'flex-wrap',
            css: [`flex-wrap: ${value}`]
        }
    }
}
