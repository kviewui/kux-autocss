/**
 * font-weight 规则
 */
export default {
    regExp: /^!?(font-weight|fw|font)-(?<value>[1-9]00|bold|bolder|inherit|initial|lighter|normal|unset)!?$/,
    render: function ({ groups }) {
        const { value } = groups;
        return {
            name: 'font-weight',
            css: [`font-weight: ${value}`]
        }
    }
}
