/**
 * cursor 规则
 */
import { CURSOR_STR } from '../constant';

export default {
    regExp: new RegExp(`^(?<important1>!?)?cursor-(?<value>${CURSOR_STR})?(?<important2>!?)$`),
    render({ groups }) {
        const { value } = groups;
        return {
            name: 'cursor',
            css: [`cursor: ${value}`]
        }
    },
}
