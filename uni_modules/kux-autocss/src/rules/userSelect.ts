/**
 * userSelect 规则
 */
export default {
    regExp: /^!?(user-)?select-(?<value>none|auto|text|all|contain|element)!?$/,
    render({ groups }) {
        const { value } = groups
        return {
            name: 'userSelect',
            css: [`user-select: ${value}`]
        }
    }
}
