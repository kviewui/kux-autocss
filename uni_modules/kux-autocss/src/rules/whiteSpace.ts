/**
 * white-space 规则
 */
import { WHITE_SPACE_VALUE_STR } from '../constant'

export default {
	regExp: new RegExp(`^!?(whitespace|ws)-(?<value>${WHITE_SPACE_VALUE_STR})!?$`),
	render({ groups }) {
		const { value } = groups
		
		return {
			name: 'whiteSpace',
			css: [`white-space: ${value}`]
		}
	}
}