/**
 * text-decoration-thickness 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant';

export default {
	regExp: new RegExp(`^!?decoration-(?<num>(auto|from-font|${NONNEGATIVE_NUMBER_REGEX_STR}))(?<unit>${UNIT_STR})?!?$`),
	render({ groups }) {
		const { num = 'auto', unit } = groups
		
		return {
			name: 'textDecorationThickness',
			css: [`text-decoration-thickness: ${num}${!['auto', 'from-font'].includes(num) ? unit : ''}`]
		}
	}
}