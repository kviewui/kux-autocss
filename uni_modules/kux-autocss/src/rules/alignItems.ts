/**
 * align-items 规则
 */
import { ALIGN_ITEMS_STR } from '../constant';

export default {
    regExp: new RegExp(`^(?<important1>!?)?(items|align-items)-(?<align>${ALIGN_ITEMS_STR})?(?<important2>!?)$`),
    render: function ({ groups }) {
        let { align } = groups;
		if (['start', 'flex-start'].includes(align)) {
			align = 'flex-start'
		}
		if (['end', 'flex-end'].includes(align)) {
			align = 'flex-end'
		}
        return { name: 'alignItems', css: [`align-items: ${align}`]};
    }
}
