/**
 * transitionDuration 规则
 */
export default {
	regExp: new RegExp(`^!?duration-(?<num>\\d+)!?$`),
	render({ groups }) {
		const { num } = groups
		
		return {
			name: 'transitionDuration',
			css: [`transition-duration: ${num}ms`]
		}
	}
}