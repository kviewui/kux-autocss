import { FLEX_DIRECTION_NAME_STR, FLEX_DIRECTION_STR } from "../constant";

/**
 * flex-direction 规则
 */
export default {
    regExp: /^!?(flex-direction|flex)-(?<value>row|row-reverse|column|col|column-reverse|col-reverse)!?$/,
	// regExp: new RegExp(`^(${FLEX_DIRECTION_NAME_STR})-(?<value>${FLEX_DIRECTION_STR})`),
    render({ groups }) {
        let { value } = groups;
		value = value.replace('col', 'column')
        return {
            name: 'flex-direction',
            css: [`flex-direction: ${value}`]
        }
    },
}
