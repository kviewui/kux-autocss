/**
 * overflow 规则
 */
export default {
    regExp: /^!?(overflow|of)(-(?<direction>[xy]))?-(?<value>hidden|auto|visible|scroll|inherit|clip)!?$/,
    render({ groups }) {
        const { direction, value } = groups
        const base = { name: 'overflow' }
        if (!direction) {
            return { ...base, css: [`overflow: ${value}`] }
        }
        if (direction === 'x') {
            return { ...base, css: [`overflow-x: ${value}`] }
        }
        if (direction === 'y') {
            return { ...base, css: [`overflow-y: ${value}`] }
        }
    }
}
