/**
 * flex 规则
 */
export default {
    regExp: /^!?flex-(?<value>null|auto|none|(0|[1-9]\d*))!?$/,
    render({ groups }) {
        let { value, extName } = groups;
		
		if (value == 1) {
			if (['nvue', 'uvue'].includes(extName)) {
				value = 1
			} else {
				value = '1 1 0%'
			}
		}

        return {
            name: 'flex',
            css: [`flex: ${value}`]
        }
    },
}
