/**
 * box-sizing 规则
 */
export default {
    regExp: /^(?<important1>!?)?box-(?<value>content|border)?(?<important2>!?)$/,
    render: function ({ groups }) {
        const { value } = groups;
        return {
            name: 'boxSizing',
            css: [`box-sizing: ${value}-box`]
        }
    }
}
