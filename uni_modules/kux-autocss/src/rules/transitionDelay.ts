/**
 * transition-delay 规则
 */
export default {
	regExp: new RegExp(`^!?(transition-delay|delay)-(?<num>\\d+)!?$`),
	render({ groups }) {
		const { num } = groups
		
		return {
			name: 'transitionDelay',
			css: [`transition-delay: ${num}ms`]
		}
	}
}