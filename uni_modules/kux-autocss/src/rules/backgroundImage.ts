/**
 * background-image 规则
 */
import { getColorValue, getColorsKey } from '../tools/colorUtils';
import { GRADIENT_DIRECTION_MAP } from '../constant'

export default {
	regExp: new RegExp(`^(?<important1>!?)?bg-(gradient|lg)-(?<direction>[trbl]{1,2})-(?<shape>(#?([a-fA-F0-9]{8}$|[a-fA-F0-9]{6}|[a-fA-F0-9]{3}))|${getColorsKey().join('|')})(-(?<shapeOpacity>1|([1-9]\\d?)))?-(?<stops>(#?([a-fA-F0-9]{8}$|[a-fA-F0-9]{6}|[a-fA-F0-9]{3}))|${getColorsKey().join('|')})(-(?<stopsOpacity>1|([1-9]\\d?)))?(?<important2>!?)?$`),
	render({ groups }) {
		const { direction = 'r', shape = 'white', shapeOpacity = 100, stops = 'white', stopsOpacity = 100 } = groups
		
		const directionArr = [...direction]
			.reduce((ac, cu) => {
				return [...ac, GRADIENT_DIRECTION_MAP.get(cu)]
			}, [])
		
		return {
			name: 'backgroundImage',
			css: [`background-image: linear-gradient(to ${directionArr.join(' ')}, ${getColorValue(shape, shapeOpacity)}, ${getColorValue(stops, stopsOpacity)})`]
		}
	}
}