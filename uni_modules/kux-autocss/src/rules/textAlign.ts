import { TEXT_ALIGN_NAME_STR, TEXT_ALIGN_STR } from "../constant"

/**
 * text-align 规则
 */
export default {
    // regExp: /^(text-align|text)-(?<value>start|end|left|right|center|justify|match-parent)$/,
	regExp: new RegExp(`^!?(${TEXT_ALIGN_NAME_STR})-(?<value>${TEXT_ALIGN_STR})!?$`),
    render({ groups }) {
        const { value } = groups
        return {
            name: 'textAlign',
            css: [`text-align: ${value}`]
        }
    }
}
