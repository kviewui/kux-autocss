/**
 * flex-basis 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant';

export default {
    regExp: new RegExp(`^!?(flex-basis|basis)-(?<value>((?<num>${NONNEGATIVE_NUMBER_REGEX_STR}|full)(?<unit>${UNIT_STR})?)|initial|inherit|auto)!?$`),
    render: function ({ groups }) {
        let { value, num, unit } = groups;
        if (num) {
            value = `${num}${unit}`;
			if (num === 'full') {
				value = '100%';
			}
        }
        return {
            name: 'flex-basis',
            css: [`flex-basis: ${value}`]
        }
    }
}
