/**
 * transitionProperty 规则
 */
import { TRANSITION_PROPERTY_VALUE_STR } from '../constant'

export default {
	regExp: new RegExp(`^!?transition(-?(?<property>(${TRANSITION_PROPERTY_VALUE_STR})?))!?$`),
	render({ groups }) {
		const { property } = groups
		
		let propertyValue = property
		
		if (!property) {
			propertyValue = 'color, background-color, border-color, text-decoration-color, fill, stroke, opacity, box-shadow, transform, filter, backdrop-filter'
		}
		
		if (property === 'colors') {
			propertyValue = 'color, background-color, border-color, text-decoration-color, border-top-color, border-bottom-color, border-left-color, border-right-color'
		}
		
		if (property === 'orientation') {
			propertyValue = 'top, bottom, left, right'
		}
		
		let css = [`transition-property: ${propertyValue}`, 'transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1)', 'transition-duration: 150ms']
		
		if (property === 'none') {
			css = [`transition-property: ${propertyValue}`]
		}
		
		return {
			name: 'transitionProperty',
			css: css
		}
	}
}