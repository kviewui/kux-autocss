/**
 * text-overflow 规则
 */
import { TEXT_OVERFLOW_VALUE_STR } from '../constant/index'

export default {
	regExp: new RegExp(`^!?(((text|to)-(?<value>${TEXT_OVERFLOW_VALUE_STR}))|(?<value2>truncate))!?$`),
	render ({ groups }) {
		const { value, extName, value2 } = groups
		
		let base: string[] = []
		
		if (value2 === 'truncate') {
			base.push(`overflow: hidden`, `text-overflow: ellipsis`)
			if (extName !== 'nvue') {
				base.push(`white-space: nowrap`)
			}
		} else {
			base.push(`text-overflow: ${value}`)
		}
		
		return {
			name: 'textOverflow',
			css: base
		}
	}
}