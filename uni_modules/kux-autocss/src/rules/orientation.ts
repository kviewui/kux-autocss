/**
 * direction 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR, DIRECTION_MAP } from '../constant'

export default {
    regExp: new RegExp(`^!?(inset-)?(?<direction>[trblxy]|top|right|bottom|left)-(?<isMinus>m-|-)?(?<num>${NONNEGATIVE_NUMBER_REGEX_STR}|a)(?<unit>${UNIT_STR})?!?$`),
    render({ groups }) {
        let { direction, isMinus, num, unit } = groups
		if (num === 'a') {
			num = 'auto'
			unit = ''
		}
        if (isMinus && num !== 'a') {
            num = 0 - num
        }
		let css = []
        // is only t r b l
        if (direction.length) {
            // direction = DIRECTION_MAP.get(direction)[0]
			css = DIRECTION_MAP
				.get(direction)
				.reduce((acc, cur) => 
					[...acc, `${cur}: ${num}${unit}`], [])
        }

        // return { name: 'orientation', num, css: [`${direction}: ${num}${unit}`] }
		return { name: 'orientation', num, css }
    }
}
