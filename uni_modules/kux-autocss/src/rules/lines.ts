/**
 * lines 规则
 */
import { NONNEGATIVE_NUMBER_REGEX_STR } from '../constant'

export default {
	regExp: new RegExp(`^!?lines-(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})!?$`),
	render({ groups }) {
		const { num } = groups
		
		return {
			name: 'lines',
			css: [`lines: ${num}`]
		}
	}
}