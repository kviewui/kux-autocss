/**
 * align-self 规则
 */
import { ALIGN_SELF_NAME_STR, ALIGN_SELF_STR } from '../constant'

export default {
	regExp: new RegExp(`^(?<important1>!?)?(${ALIGN_SELF_NAME_STR})-(?<content>${ALIGN_SELF_STR})?(?<important2>!?)$`),
	render: function({ groups }) {
		const { content } = groups
		let prefix = ''
		
		if (['start', 'end'].includes(content)) {
			prefix = 'flex-'
		}
		
		return {
			name: 'alignSelf',
			css: [`align-self: ${prefix}${content}`]
		}
	}
}