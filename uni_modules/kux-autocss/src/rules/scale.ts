/**
 * scale 规则
 */
import { SCALE_DIRECTION_MAP } from '../constant'

export default {
	regExp: new RegExp(`^!?scale(-?(?<direction>[xy])?-)(?<angle>(\\d+))!?$`),
	render({ groups }) {
		const { direction, angle } = groups
		
		return {
			name: 'scale',
			css: [`transform: ${SCALE_DIRECTION_MAP.get(direction)}(${angle / 100})`]
		}
	}
}