/**
 * vertical-align 规则
 */
import { VERTICAL_ALIGN_STR, UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant'

export default {
    regExp: new RegExp(`^!?vertical-align-(?<value>((?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?)|${VERTICAL_ALIGN_STR})!?$`),
    render({ groups }) {
        let { value, num, unit } = groups
        if (num) {
            value = `${num}${unit}`
        }
        return {
            name: 'verticalAlign',
            css: [`vertical-align: ${value}`]
        }
    }
}
