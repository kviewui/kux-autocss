/**
 * align-content 规则
 */
import { ALIGN_CONTENT_NAME_STR, ALIGN_CONTENT_STR } from '../constant'

export default {
	regExp: new RegExp(`^(?<important1>!?)?(${ALIGN_CONTENT_NAME_STR})-(?<content>${ALIGN_CONTENT_STR})?(?<important2>!?)$`),
	render: function({ groups }) {
		const { content } = groups
		let prefix = ''
		
		if (['start', 'end'].includes(content)) {
			prefix = 'flex-'
		}
		
		if (['between', 'around'].includes(content)) {
			prefix = 'space-'
		}
		
		return {
			name: 'alignContent',
			css: [`align-content: ${prefix}${content}`]
		}
	}
}