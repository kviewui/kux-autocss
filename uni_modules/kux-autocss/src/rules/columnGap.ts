/**
 * column-gap 规则
 */
import { GAP_STR, NONNEGATIVE_NUMBER_REGEX_STR, UNIT_STR } from '../constant';

export default {
    regExp: new RegExp(`^(?<important1>!?)?c(olumn|ol)?-gap-(((?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?)|(?<value>${GAP_STR}))?(?<important2>!?)$`),
    render: function ({ groups }) {
        let { num = Infinity, unit, value } = groups;
        if (!value) { value = num + unit; }
        return {
            name: 'column-gap',
            css: [`column-gap: ${value}`]
        }
    }
}
