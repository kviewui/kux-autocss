/**
 * font-size 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant';

export default {
    regExp: new RegExp(`^(?<important1>!?)?(font-size|fs|text)-(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?(?<important2>!?)$`),
	// regExp: /^(font-size|fs|text)-(?<num>\d+(\.\d+)?)(?<unit>[a-zA-Z%]*)?(?<important>!?)$/,
    render({ groups }) {
        const { num, unit } = groups;
        return {
            name: 'font-size',
            css: [`font-size: ${num}${unit}`]
        }
    },
}
