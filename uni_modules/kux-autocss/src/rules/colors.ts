/**
 * color规则
 */
import { DIRECTION_MAP } from '../constant';
import { getColorsKey, textToRgbText, getColorValue } from '../tools/colorUtils';
import { generateColors } from '../theme'

export default {
    regExp: () => new RegExp(
        `^(?<important1>!?)?(?<type>color|c|text|bg|background|border-color|border-c|border)-((?<direction>[trblxy])-)?(?<color>(#?([a-fA-F0-9]{8}$|[a-fA-F0-9]{6}|[a-fA-F0-9]{3}))|${getColorsKey().join('|')})(-(?<opacity>1|([1-9]\\d?)))?(?<important2>!?)?$`),
    render({ groups }) {
        let { type, color, themeColor, opacity, direction } = groups;
        // opacity = opacity === undefined ? 1 : (opacity * 0.01).toFixed(2);
        // color = textToRgbText(color, opacity); // rgba(xxxx) or transparent
		// console.log(generateColors());
		// let colorClassName = opacity ? `${color}-${opacity}` : color
		// const generateColorsRes = generateColors().filter(item => item.className === colorClassName)
		// console.log(color,);
		// if (generateColorsRes.length === 0) {
		// 	color = getColorValue(color, opacity)
		// }
		color = getColorValue(color, opacity)
        let prefix = '';
        switch (type) {
            case 'c':
            case 'color':
            case 'text':
                prefix = 'color';
                break;
            case 'bg':
            case 'background':
                prefix = 'background-color';
                break;
			case 'border':
            case 'border-c':
            case 'border-color':
                prefix = 'border-color';
				if (direction) {
					prefix = `border-${DIRECTION_MAP.get(direction)[0]}-color`
				}
                break;
            default:
                prefix = type;
                break;
        }

        return {
            name: 'colors',
            css: [`${prefix}: ${color}`]
        }
    },
}
