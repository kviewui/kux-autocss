/**
 * transitionTimingFunction 规则
 */
import { TRANSITION_TIMING_FUNCTION_VALUE_STR } from '../constant'

export default {
	regExp: new RegExp(`^!?(ease|transition-ease)(-?(?<value>${TRANSITION_TIMING_FUNCTION_VALUE_STR})?)!?$`),
	render({ groups }) {
		let { value } = groups
		
		if (['in', 'out', 'in-out'].includes(value)) {
			value = `ease-${value}`
		}
		
		if (!value) {
			value = 'ease'
		}
		
		return {
			name: 'transitionTimingFunction',
			css: [`transition-timing-function: ${value}`]
		}
	}
}