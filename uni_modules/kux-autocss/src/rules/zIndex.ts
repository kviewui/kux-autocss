/**
 * z-index 规则
 */
export default {
    regExp: /^!?(z-index|z)-(?<isMinus>(m-|-))?(?<value>0|[1-9]\d*)!?$/,
    render({ groups }) {
        let { isMinus, value } = groups
        if (isMinus) {
            value = 0 - value
        }
        return { name: 'zIndex', num: value, css: [`z-index: ${value}`] }
    }
}
