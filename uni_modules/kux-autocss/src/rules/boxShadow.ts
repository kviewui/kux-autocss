/**
 * box-shadow 规则
 */
import { BOX_SHADOW_SIZE_STR, BOX_SHADOW_SIZE_MAP } from '../constant'

export default {
	regExp: new RegExp(`^(?<important1>!?)?(shadow-|shadow)(?<value>${BOX_SHADOW_SIZE_STR})?(?<important2>!?)$`),
	render({ groups }) {
		const { value = '' } = groups
		
		return {
			name: 'boxShadow',
			css: [`box-shadow: ${BOX_SHADOW_SIZE_MAP.get(value)}`]
		}
	}
}