/**
 * translate 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR, TRANSLATE_DIRECTION_MAP } from '../constant';
import { isFraction } from '../tools/utils'

export default {
	regExp: new RegExp(`^!?translate(-?(?<direction>[xy])?-)(?<isMinus>m-|-)?(?<num>${NONNEGATIVE_NUMBER_REGEX_STR}|full)(?<unit>${UNIT_STR})?!?$`),
	render({ groups }) {
		let { direction, isMinus, num, unit } = groups
		
		if (num === 'full') {
			num = '100%'
			unit = ''
		}
		
		if (isFraction(num)) {
			unit = ''
		}
		
		if (isMinus && num !== 'full') {
		    num = 0 - num
		}
		
		return {
			name: 'translate',
			css: [`transform: ${TRANSLATE_DIRECTION_MAP.get(direction)}(${num}${unit})`]
		}
	}
}