/**
 * text-decoration-style 规则
 */
import { TEXT_DECORATION_STYLE_VALUE_STR } from '../constant'

export default {
	regExp: new RegExp(`^!?decoration-(?<value>${TEXT_DECORATION_STYLE_VALUE_STR})!?$`),
	render({ groups }) {
		const { value } = groups
		
		return {
			name: 'textDecorationStyle',
			css: [`text-decoration-style: ${value}`]
		}
	}
}