/**
 * transform-origin 规则
 */
import { TRANSFORM_ORIGIN_DIRECTION_MAP } from '../constant'

export default {
	regExp: new RegExp(`^!?origin-(?<direction>(${Array.from(TRANSFORM_ORIGIN_DIRECTION_MAP.keys()).join('|')}))!?$`),
	render({ groups }) {
		const { direction } = groups
		
		return {
			name: 'transformOrigin',
			css: [`transform-origin: ${TRANSFORM_ORIGIN_DIRECTION_MAP.get(direction)}`]
		}
	}
}