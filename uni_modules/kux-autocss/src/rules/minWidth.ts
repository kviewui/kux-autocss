/**
 * min-width 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant'

export default {
    regExp: new RegExp(`^!?(min-w|min-width)-(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?!?$`),
    render({ groups }) {
        const { num, unit } = groups
        return { name: 'min-width', num, css: [`min-width: ${num}${unit}`] }
    }
}
