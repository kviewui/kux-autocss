/**
 * visibility 规则
 */
import { toRegexStr } from '../constant'
const list = ['visible', 'collapse', 'inherit', 'initial', 'revert', 'unset']
export default {
    regExp: new RegExp(`^!?(visibility-|)(?<value>${toRegexStr(list)})!?$`),
    render({ groups }) {
        const { value } = groups
        return { name: 'visibility', css: [`visibility: ${value}`] }
    }
}
