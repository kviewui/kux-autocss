import { DIRECTION_MAP } from "../constant";

/**
 * border-style 规则
 */
export default {
    regExp: /^(?<important1>!?)?(border-style|border)-((?<direction>[trblxy])-)?(?<value>none|hidden|dotted|dashed|solid|double|groove|ridge|inset|outset|inherit)?(?<important2>!?)$/,
    render({ groups }) {
        const { value, direction } = groups;
		let css: string[] = []
		if (direction) {
			css =  DIRECTION_MAP.get(direction)
				.reduce((t: any, c: any) => {
					return [...t, `border-${c}-style: ${value}`]
				}, [])
		} else {
			css = [`border-style: ${value}`]
		}
        return {
            name: 'borderStyle',
            css: css
        }
    },
}
