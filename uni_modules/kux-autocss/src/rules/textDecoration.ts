/**
 * text-decoration 规则
 */
export default {
    regExp: /^!?(text-decoration|text)-(?<value>none|underline|overline|line-through|blink|inherit)!?$/,
    render({ groups }) {
        const { value } = groups
        return {
            name: 'textDecoration',
            css: [`text-decoration-line: ${value}`]
        }
    }
}
