/**
 * letter-spacing 规则
 */
import { UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR } from '../constant';

export default {
    regExp: new RegExp(`^!?(letter-spacing|tracking)-(?<isMinus>(m-|-))?(?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?!?$`),
    render: function ({ groups }) {
        let { isMinus, num, unit } = groups;
        if (isMinus) {
            num = 0 - num;
        }

        return {
            name: 'letter-spacing',
            num,
            css: [`letter-spacing: ${num}${unit}`]
        }
    }
}
