/**
 * flexFlow 规则
 */
import { FLEX_FLOW_NAME_STR, FLEX_FLOW_DIRECTION_STR, FLEX_FLOW_WRAP_STR } from '../constant'

export default {
	regExp: new RegExp(`^!?${FLEX_FLOW_NAME_STR}-(?<direction>${FLEX_FLOW_DIRECTION_STR})-(?<wrap>${FLEX_FLOW_WRAP_STR})!?$`),
	render({ groups }) {
		let { direction, wrap } = groups
		
		if (['col-reverse', 'col'].includes(direction)) {
			direction = direction.replace('col', 'column')
		}
		
		return {
			name: 'flexFlow',
			css: [`flex-flow: ${direction} ${wrap}`]
		}
	}
}