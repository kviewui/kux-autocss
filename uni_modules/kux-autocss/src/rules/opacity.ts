/**
 * opacity 规则
 */
export default {
    regExp: /^!?(opacity|op)-(?<value>([1-9]?\d|100))!?$/,
    render({ groups }) {
        const { value } = groups
        return { name: 'opacity', order: Infinity, num: value, css: [`opacity: ${Number((value / 100).toFixed(2))}`] }
    }
}
