/**
 * justify-content 规则
 */
import { JUSTIFY_CONTENT_STR } from '../constant';

export default {
    regExp: new RegExp(`^!?(justify|justify-content)-(?<justify>${JUSTIFY_CONTENT_STR})!?$`),
    render({ groups }) {
        let { justify } = groups;
		if (['between', 'space-between'].includes(justify)) {
			justify = 'space-between'
		}
		if (['start', 'flex-start'].includes(justify)) {
			justify = 'flex-start'
		}
		if (['end', 'flex-end'].includes(justify)) {
			justify = 'flex-end'
		}
		if (['around', 'space-around'].includes(justify)) {
			justify ='space-around'
		}
        return {
            name: 'justify-content',
            css: [`justify-content: ${justify}`]
        }
    },
}
