/**
 * row-gap 规则
 */
import { GAP_STR, NONNEGATIVE_NUMBER_REGEX_STR, UNIT_STR } from '../constant'

export default {
    regExp: new RegExp(`^!?r(ow)?-gap-(((?<num>${NONNEGATIVE_NUMBER_REGEX_STR})(?<unit>${UNIT_STR})?)|(?<value>${GAP_STR}))!?$`),
    render({ groups }) {
        let { num = Infinity, unit, value } = groups
        if (!value) { value = num + unit }
        return { name: 'row-gap', num, css: [`row-gap: ${value}`] }
    }
}
