/**
 * text-decoration-color 规则
 */
import { getColorsKey, textToRgbText, getColorValue } from '../tools/colorUtils';

export default {
	regExp: new RegExp(`^!?decoration-(?<color>(#?([a-fA-F0-9]{8}$|[a-fA-F0-9]{6}|[a-fA-F0-9]{3}))|${getColorsKey().join('|')})(-(?<opacity>1|([1-9]\\d?)))?!?$`),
	render({ groups }) {
		const { color, opacity } = groups
		
		const colorValue = getColorValue(color, opacity)
		
		return {
			name: 'textDecorationColor',
			css: [`text-decoration-color: ${colorValue}`]
		}
	}
}