/**
 * rotate 规则
 */
import { ROTATE_DIRECTION_MAP } from '../constant'

export default {
	regExp: new RegExp(`!?rotate(-?(?<direction>[xyz])?-)(?<isMinus>m-|-)?(?<deg>(\\d+))!?$`),
	render({ groups }) {
		let { direction, isMinus, deg } = groups
		
		if (isMinus) {
			deg = -deg
		}
		
		return {
			name: 'rotate',
			css: [`transform: ${ROTATE_DIRECTION_MAP.get(direction)}(${deg}deg)`]
		}
	}
}