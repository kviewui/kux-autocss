import { V_TO_ANY, PREFIX, PREFIX_SEPARATOR } from '../constant';
import { getConfig } from '../config';
import type { UserConfig } from 'vite'

const accDiv = (arg1: { toString: () => string; }, arg2: { toString: () => string; }) => {
    let t1 = 0
    let t2 = 0
    try {
        t1 = arg1.toString().split('.')[1].length
    } catch (e) {
        console.error(e)
    }
    try {
        t2 = arg2.toString().split('.')[1].length
    } catch (e) {
        console.error(e)
    }
    const r1 = Number(arg1.toString().replace('.', ''))
    const r2 = Number(arg2.toString().replace('.', ''))
    return (r1 / r2) * Math.pow(10, t2 - t1)
}

export const isFunction = (payload: any) => {
    return Object.prototype.toString.call(payload) === '[object Function]';
}

export const isObject = (payload: any) => {
    return Object.prototype.toString.call(payload) === '[object Object]';
}

/**
 * 判断是否是分数格式
 * 
 * @param num 字符串数字
 * 
 * @returns boolean
 */
export const isFraction = (num: string) => {
	const fractionRegex = /^-?\d+(\.\d+)?\/-?\d+(\.\d+)?$/
	return fractionRegex.test(num)
}

export function fractionToPercent(fractionStr: string): string {
  // 使用正则表达式匹配分子和分母
  const match = fractionStr.match(/^(-?\d+(?:\.\d+)?)(?:\/)(-?\d+(?:\.\d+)?)$/);
  if (!match) {
    throw new Error('Invalid fraction format');
  }

  // 提取分子和分母
  const numerator = parseFloat(match[1]);
  const denominator = parseFloat(match[2]);

  // 检查分母是否为零
  if (denominator === 0) {
    throw new Error('Denominator cannot be zero');
  }

  // 计算分数值
  const value = numerator / denominator;

  // 转换为百分数并保留10位小数
  const percent = (value * 100).toFixed(10);

  // 返回带有百分号的字符串
  return `${percent}`;
}

export const groupBy = (array: any[], name: string | number) => {
    const groups = {}
    array.forEach(function (o: { [x: string]: any; }) {
        const group = JSON.stringify(o[name])
        groups[group] = groups[group] || []
        groups[group].push(o)
    })
    return Object.keys(groups).map(function (group) {
        return groups[group]
    })
}

export const getDirectionOrder = (order: number, direction: any) => {
    if (!direction) { return order }
    switch (direction) {
        case 'x':
            return order + 10
        case 'y':
            return order + 20
        case 't':
            return order + 30
        case 'b':
            return order + 40
        case 'r':
            return order + 50
        case 'l':
            return order + 60
    }
}

export const v2any = (num: number) => {
    num = Number(num)
    const { rootValue = 16, unitPrecision = 5, minPixelValue = 1 } = getConfig(V_TO_ANY)
    if (num < minPixelValue) { return num }
    return Number(accDiv(num, rootValue).toFixed(unitPrecision))
}

export const prefixStr = (isPrefix: string | boolean = getConfig(PREFIX)): string => {
    return isPrefix ? `${getConfig(PREFIX)}${getConfig(PREFIX_SEPARATOR)}` : '';
}

const regex = /^\\!|!$/;
export const toRegexImportant = (className: string) => regex.test(className);

export const getExtName = (url: string) => {
    // const ext = url.split('.').pop();
    // return ext?.startsWith('.') ? ext.substring(1) : ext;
	// 使用path.extname获取文件后缀名，并去除前导的'.'
	const path = require('path');
	// 移除查询字符串（如果存在）
	const pathWithoutQuery = url.split('?')[0];
	  
	// 使用path.extname获取文件后缀名，并去除前导的'.'
	const extension = path.extname(pathWithoutQuery).substring(1);
	  
	return extension;
}

export const getRoot = (config: UserConfig) => {
	return process.env.UNI_INPUT_DIR || config.root || process.cwd()
}
