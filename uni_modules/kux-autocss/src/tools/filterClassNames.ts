import { getConfig, getUnit } from '../config';
import { pushPreObj, pushQuery } from './preRender';

import {
    GLOB_REG,
    BASE_MEDIA_QUERY_KEY,
    OVERRIDE_RULES,
    MEDIA_QUERIES,
    PSEUDO_STR,
    V_TO_ANY,
    UNIT_STR,
    NONNEGATIVE_NUMBER_REGEX_STR,
    DIRECTION_MAP,
    PREFIX,
    PREFIX_SEPARATOR
} from '../constant';

import * as rules from '../rules';
import { v2any, isFunction, isObject, isFraction, fractionToPercent } from './utils';
import { textToRgbText, getColorsKey, getColors } from './colorUtils';
import { generateShortcuts } from './shortcuts';

const modifyUtils = { textToRgbText, getColorsKey, getColors, UNIT_STR, NONNEGATIVE_NUMBER_REGEX_STR, DIRECTION_MAP };

const cssSet = new Set();
const handleCssPipe = new Set();
let currentExtName: string = '';
let shortCutObj = new Map()

const hasShortcut = (shortcuts) => {
	if (!shortcuts) 
		return false
	if (isObject(shortcuts)) 
		return 'object'
	if (Array.isArray(shortcuts)) 
		return 'array'
	return false
}

const filterShortCutObject = (shortcut, item) => {
	if (Object.keys(shortcut).includes(item)) {
		const classArr = shortcut[item].split(' ')
		shortCutObj.set(item, shortcut[item])
		filterCustomClassByName(item, classArr)
	} else {
		filterClass(item)
	}
}

export const filterClassNames = (str, extName = 'uvue') => {
    // console.log(getConfig(GLOB_REG));
	currentExtName = extName
    let classNameList = str.match(getConfig(GLOB_REG)) ?? [];
	const shortcuts = getConfig('shortcuts')

    if (classNameList) {
        classNameList.forEach(hasClassNameStr => {
            // 替换规则中没有出现的字符为空格
            const className = hasClassNameStr.replace(/[^a-zA-Z0-9-@:#./!]/g, ' ');
            // className.split(' ').forEach(filterClass);
			className.split(' ').forEach(item => {
				if (hasShortcut(shortcuts) === 'object') {
					filterShortCutObject(shortcuts, item)
				} else if (hasShortcut(shortcuts) === 'array') {
					shortcuts.forEach(shortcut => {
						if (isObject(shortcut)) {
							filterShortCutObject(shortcut, item)
						} else if (Array.isArray(shortcut)) {
							const [pattern, replacer] = shortcut
							if (pattern.test(item)) {
								const generatedClassStr = item.replace(pattern, replacer)
								let dymamicShortcut = {}
								dymamicShortcut[item] = ''
								generatedClassStr.split(' ').forEach(classItem => {
									if (shortCutObj.get(classItem)) {
										dymamicShortcut[item] += shortCutObj.get(classItem)
									} else {
										dymamicShortcut[item] += ' ' + classItem
									}
								})
								if (dymamicShortcut[item]) {
									filterShortCutObject(dymamicShortcut, item)
								}
							}
						}
					})
				} else {
					filterClass(item)
				}
			})
        })
    }
    return '';
}

export const filterCustomClassByName = (className: string, classArr: string[]) => {
	let isPrefixStr: boolean = false;
	let cssArr: string[] = [];
	let queryCssArr: {
		query: string
		css: string[]
	}[] = []
	
	classArr.forEach(classStr => {
		let query; let pseudo; let source = classStr;
		const queryNames = [...BASE_MEDIA_QUERY_KEY, ...Object.keys(getConfig(MEDIA_QUERIES))];
		const v2anyConfig = getConfig(V_TO_ANY);
		const isV2any = isObject(v2anyConfig);
		
		if (/[@:]/.test(classStr)) {
			const queryAndPesudoRegex = new RegExp(`^(?:(?<query>${queryNames.join('|')})@)?(?:(?<pseudo>${PSEUDO_STR}):)?(?<source>[^:@]+)$`)
			const res = classStr.match(queryAndPesudoRegex)
			if (!res) {
				return null
			}
			const { groups = null } = res
			if (!groups) {
				return null
			}
			({ query, pseudo, source } = groups)
		}
		
		const ruleList = Object.values({ ...rules, ...getConfig(OVERRIDE_RULES) });
		
		for (let i = 0; i < ruleList.length; i++) {
		    let rule: any = ruleList[i]
		    rule = isFunction(rule) ? rule(modifyUtils) : rule
		    const reg = isFunction(rule.regExp) ? rule.regExp() : rule.regExp
		    const res = source.match(reg)
		    if (res !== null) {
		        let { unit, num } = res.groups || {}
				if (isFraction(num)) {
					unit = 'p'
					num = fractionToPercent(num)
					if (res.groups && isObject(res.groups)) {
						res.groups.num = num
					}
				}
		        const unit1 = getUnit(num, unit)
		
		        if (res.groups && isObject(res.groups)) {
		            if (isV2any && unit1 === 'v' && num) {
		                Object.assign(res.groups, {
		                    num: v2any(parseInt(num)),
		                    unit: v2anyConfig.unit
		                })
		            } else {
		                Object.assign(res.groups, { unit: unit1 })
		            }
		        }
				
				if (res.groups) {
					res.groups.extName = currentExtName
				}
				
		        let renderResult = rule.render(res)
		
		        handleCssPipe.forEach((handle: any) => {
		            renderResult = handle(renderResult)
		        })
				
				if (query) {
					queryCssArr.push({
						query: query,
						css: [...renderResult.css]
					})
				} else {
					cssArr.push(...renderResult.css)
				}
		    }
		}
	})
	
	let params = {
		classStr: className,
		isPrefixStr,
		name: 'shortcuts',
		css: [] as string[]
	}
	
	params.classStr = params.classStr.replace(/\//g, '\\/')
	params.classStr = params.classStr.replace(/\!/g, '\\!')
	
	if (classArr.length > 0) {
		params.css = [...cssArr]
	}
	
	pushPreObj(params)
}

export const filterClass = (classStr) => {
    let newClassStr: any = classStr;
    let isPrefixStr: boolean = false;
    if (getConfig(PREFIX).length > 0) {
        let str = classStr.split(`${getConfig(PREFIX)}${getConfig(PREFIX_SEPARATOR)}`)[1];

        if (str) {
            isPrefixStr = true;
            newClassStr = str;
        }
    }
    if (cssSet.has(newClassStr)) {
        return null;
    }
    let query; let pseudo; let source = newClassStr;
    const queryNames = [...BASE_MEDIA_QUERY_KEY, ...Object.keys(getConfig(MEDIA_QUERIES))];
    const v2anyConfig = getConfig(V_TO_ANY);
    const isV2any = isObject(v2anyConfig);

    if (/[@:]/.test(newClassStr)) {
        const queryAndPesudoRegex = new RegExp(`^(?:(?<query>${queryNames.join('|')})@)?(?:(?<pseudo>${PSEUDO_STR}):)?(?<source>[^:@]+)$`)
        const res = newClassStr.match(queryAndPesudoRegex)
        if (!res) {
            return null
        }
        const { groups = null } = res
        if (!groups) {
            return null
        }
        ({ query, pseudo, source } = groups)
    }

    cssSet.add(newClassStr)

    const ruleList = Object.values({ ...rules, ...getConfig(OVERRIDE_RULES) });

    for (let i = 0; i < ruleList.length; i++) {
        let rule: any = ruleList[i]
        rule = isFunction(rule) ? rule(modifyUtils) : rule
        const reg = isFunction(rule.regExp) ? rule.regExp() : rule.regExp
        const res = source.match(reg)
		
        if (res !== null) {
            let { unit, num } = res.groups || {}
			if (isFraction(num)) {
				unit = 'p'
				num = fractionToPercent(num)
				res.groups.num = num
			}
            const unit1 = getUnit(num, unit)

            if (isObject(res.groups)) {
                if (isV2any && unit1 === 'v' && num) {
                    Object.assign(res.groups, {
                        num: v2any(num),
                        unit: v2anyConfig.unit
                    })
                } else {
                    Object.assign(res.groups, { unit: unit1 })
                }
            }
			
			if (res.groups) {
				res.groups.extName = currentExtName
			}
			
            let renderResult = rule.render(res)

            handleCssPipe.forEach((handle: any) => {
                renderResult = handle(renderResult)
            })

            const params = {
                classStr: newClassStr,
                isPrefixStr,
                ...renderResult,
                pseudo
            }
			
			params.classStr = params.classStr.replace(/\//g, '\\/')
			params.classStr = params.classStr.replace(/\!/g, '\\!')
			
            if (query) {
                pushQuery(query, params)
            } else {
                pushPreObj(params)
            }
            break
        }
    }
}
