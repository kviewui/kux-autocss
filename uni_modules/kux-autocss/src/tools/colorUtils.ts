import { getConfig } from '../config';
import { COLORS } from '../constant';

const colorStore = () => ({
    red: '#F53F3F',
    orangered: '#F77234',
    orange: '#FF7D00',
    gold: '#F7BA1E',
    yellow: '#FADC19',
    lime: '#9FDB1D',
    green: '#00BC79',
    cyan: '#14C9C9',
    blue: '#3491FA',
    deepblue: '#165DFF',
    purple: '#722ED1',
    pinkpurple: '#D91AD9',
    magenta: '#F5319D',
    grey: '#86909c',
    black: '#000',
    white: '#FFF',
    transparent: 'transparent',
    ...getConfig(COLORS)
});

export const getColors = () => {
    return colorStore();
}

export const getColorsKey = () => {
    return Object.keys(colorStore());
}

const radix16 = (value: string) => {
    return parseInt(value, 16);
}

export const textToRgbText = (str: any, opacity = 1) => {
    const hex = /^#?([a-fA-F0-9]{8}|[a-fA-F0-9]{6}|[a-fA-F0-9]{3})$/.test(str)
        ? str.replace(/^#/, '')
        : colorStore()[str].replace(/^#/, '')
    
    if (hex === 'transparent') {
        return 'transparent';
    }

    if (hex.length === 6) {
        const reg = /[a-fA-F0-9]{2}/g;
        return 'rgba(' + hex
            .match(reg)
            .map(radix16)
            .join(',') + 
            `,${opacity})`
    }

    if (hex.length === 3) {
        return 'rgba(' + hex
            .split('')
            .map((x: string) => radix16(x.repeat(2)))
            .join(',') + 
            `,${opacity})`
    }

    if (hex.length === 8) {
        const reg = /[a-fA-F0-9]{2}/g;
        let [r, g, b, a] = hex.match(reg);
        a = Number(Number(Math.round(parseInt(a, 16)) / 255).toFixed(2));
        return 'rgba(' + [r, g, b]
            .map(radix16)
            .join(',') +
            `,${a})`;
    }

    return '';
}

/**
 * 根据颜色名称和透明度获取颜色值
 */
export const getColorValue = (color: string, opacity = 100) => {
    const opacityValue = (opacity * 0.01).toFixed(2)
	
    return textToRgbText(color, Number(opacityValue));
}

/**
 * @description console控制台输入彩色文字，可参考node控制台API
 * @param input 输出的文字内容
 * @param color_code 颜色代码，具体如下：
 * 	+ 30 黑色
 * 	+ 31 红色
 * 	+ 32 绿色
 * 	+ 33 黄色
 * 	+ 34 蓝色
 * 	+ 35 紫色
 * 	+ 36 青色
 * 	+ 37 白色
 * 	+ 90 亮黑
 * 	+ 91 亮红
 * 	+ 92 亮绿
 * 	+ 93 亮黄
 * 	+ 94 亮蓝
 * 	+ 95 亮紫
 * 	+ 96 亮青
 * 	+ 97 亮白
 * @see https://zh.wikipedia.org/wiki/ANSI%E8%BD%AC%E4%B9%89%E5%BA%8F%E5%88%97
 * @returns 
 */
export const changeConsoleColor = (input: string, color_code: number = 92, bg_color_code: number = 105) => {
    // return `\x1b[${color_code}m${input}\x1b[0m`;
	const bg_color_code_str = bg_color_code > 0 ? `;${bg_color_code}` : '';
	const color_code_str = `${color_code}`;
	if (bg_color_code > 0) {
		return `\x1b[${color_code_str}${bg_color_code_str}m${input}\x1b[0m`;
	}
	
	return `\x1b[${color_code_str}m${input}\x1b[0m`;
}

/**
 * 设置控制台打印的文字状态
 * @param {string} input 文字内容
 * @param {'success' | 'danger' | 'warning' | 'info'} theme 设置主题
 * + success 成功状态【绿色】
 * + danger 失败状态【红色】
 * + warning 警告状态【黄色】
 * + link 链接状态【蓝色】
 * + info 信息状态【白色】
 * @return 
 * @example
 * ```ts
 * consolePrint('成功状态文字', 'success');
 * consolePrint('失败状态文字', 'danger');
 * consolePrint('警告状态文字', 'warning');
 * consolePrint('链接状态文字', 'link');
 * consolePrint('信息状态文字', 'info');
 * ```
 */ 
export function consolePrint (input: string, theme: 'success' | 'danger' | 'warning' | 'link' | 'info' = 'info') {
	switch (theme) {
		case 'danger':
			console.info(changeConsoleColor(input, 31));
			// console.error(input);
			break;
		case 'success':
			console.info(changeConsoleColor(input, 32));
			break;
		case 'warning':
			console.info(changeConsoleColor(input, 33));
			break;
		case 'link':
			console.info(changeConsoleColor(input, 34));
			break;
		default:
			console.info(changeConsoleColor(input, 37));
	}
}