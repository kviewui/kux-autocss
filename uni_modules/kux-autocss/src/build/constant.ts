import { PluginOptions } from 'types';
import { version, repository, author } from '../../package.json';

export const PACKAGE_VERSION = version;
export const GIT_REPOSITORY_URL = repository ?? 'https://gitcode.com/kviewui/kux-autocss';
export const AUTHOR = author ?? 'kux <kviewui@163.com>';
export const NOW_DATE = formatDate(new Date());

export function formatDate (date: Date): string {
	const year = date.getFullYear();
	const month = (date.getMonth() + 1).toString().padStart(2, '0');
	const day = date.getDate().toString().padStart(2, '0');
	const hours = date.getHours().toString().padStart(2, '0');
	const minutes = date.getMinutes().toString().padStart(2, '0');
	const seconds = date.getSeconds().toString().padStart(2, '0');
	
	return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

const timestamp : number = Date.now();
export const date : Date = new Date(timestamp);

export const CONFIG_CONTENT = (pluginOptions?: PluginOptions) => {
	let str = ''
	
	str += '/**\n'
	str += '* @description autocss config file\n'
	str += `* @version v${PACKAGE_VERSION}\n`
	str += `* @author ${AUTHOR}\n`
	str += `* @see ${GIT_REPOSITORY_URL}\n`
	str += `* @date ${formatDate(date)}\n`
	str += `* @copyright Copyright (c) ${date.getFullYear()} The Authors.\n`
	str += `* @license MIT License\n`
	str += `* Permission is hereby granted, free of charge, to any person obtaining a copy\n`
	str += `* of this software and associated documentation files (the "Software"), to deal\n`
	str += `* in the Software without restriction, including without limitation the rights\n`
	str += `* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell\n`
	str += `* copies of the Software, and to permit persons to whom the Software is\n`
	str += `* furnished to do so, subject to the following conditions:\n`
	str += `* \n`
	str += `* The above copyright notice and this permission notice shall be included in all\n`
	str += `* copies or substantial portions of the Software.\n`
	str += `* \n`
	str += `* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR\n`
	str += `* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,\n`
	str += `* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE\n`
	str += `* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER\n`
	str += `* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,\n`
	str += `* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE\n`
	str += `* SOFTWARE.\n`
	str += `**/\n`
	str += `module.exports = {\n`
	str += `\tbeforeContent   : '${pluginOptions?.beforeContent ?? ''}', // css文件头部文本\n`
	str += `\tafterContent    : '${pluginOptions?.afterContent ?? ''}', // css文件结束的文本\n`
	str += `\tprefix          : '${pluginOptions?.prefix ?? ''}', // css前缀\n`
	str += `\tprefixSeparator : '${pluginOptions?.prefixSeparator ?? ''}', // css前缀分隔符，比如css前缀为kui，分隔符为-的话将自动生成类似kui-c-red格式的代码\n`
	str += `\tautoUseSnippets : ${pluginOptions?.autoUseSnippets || true}, // 是否自动生成css代码提示文件\n`
	str += `\tcssSnippetsFile : '${pluginOptions?.cssSnippetsFile ?? 'autocss.snippets.css'}', // CSS代码提示文件位置\n`
	str += `\t/**\n`
	str += `\t * 颜色配置 默认包含如下值\n`
	str += `\t * red          : '#F53F3F'\n`
	str += `\t * oranged      : '#F77234'\n`
	str += `\t * orange       : '#FF7D00'\n`
	str += `\t * gold         : '#F7BA1E'\n`
	str += `\t * yellow       : '#FADC19'\n`
	str += `\t * lime         : '#9FDB1D'\n`
	str += `\t * green        : '#00BC79'\n`
	str += `\t * cyan         : '#14C9C9'\n`
	str += `\t * blue         : '#3491FA'\n`
	str += `\t * deepblue     : '#165DFF'\n`
	str += `\t * purple       : '#722ED1'\n`
	str += `\t * pinkpurple   : '#D91AD9'\n`
	str += `\t * magenta      : '#F5319D'\n`
	str += `\t * grey         : '#86909C'\n`
	str += `\t * black        : '#000000'\n`
	str += `\t * white        : '#FFFFFF'\n`
	str += `\t * transparent  : 'transparent'\n`
	str += `\t * 只需要配置颜色的变量值即可，会自动适配到所有颜色相关属性，如 color-red bg-red bg-diy\n`
	str += `\t */\n`
	str += `\t// colors       : {},\n`
	str += `\troot            : '${pluginOptions?.root ?? './'}', // 必填项，源码根目录\n`
	str += `\tcssFile          : '${pluginOptions?.cssFile ?? 'style/auto.css'}', // 必填项，自动生成的css文件位置（不存在会自动创建目录【hbx 可视化创建的项目暂时不会自动创建，需要手动创建好目录】）\n`
	str += `\ttype            : 'uniapp', // 必填项，项目类型，暂时固定 uniapp类型\n`
	str += `\textName         : ${pluginOptions?.extName ?? '[\'vue\', \'nvue\', \'uvue\']'}, // 可选项，自动生成css时检索的文件类型，可根据自己的项目自由调整，不过需注意为数组类型。\n`
	str += `\tunit            : '${pluginOptions?.unit ?? 'px'}', // 可选项，默认单位 px 如填 v 则必须配合 vToAny\n`
	str += `\t\n`
	str += `\t/**\n`
	str += `\t * 自定义规则，可复写默认规则\n`
	str += `\t */\n`
	str += `\toverrideRules   : ${pluginOptions?.overrideRules ?? '\{\}'},\n`
	str += `\t\n`
	str += `\tinclude         : ${pluginOptions?.include ? JSON.stringify(pluginOptions.include) : '[]'}, // 可选项，需要处理的文件列表，支持glob语法，如 ['src/**/*.vue', '!src/components/ignore/**'], 默认规则：['pages/**']\n`
	str += `\texclude         : ${pluginOptions?.exclude ? JSON.stringify(pluginOptions.exclude) : '[]'}, // 可选项，不需要处理的文件列表，支持glob语法，如 ['src/components/ignore/**'],\n\n`
	str += `\t/**\n`
	str += `\t * 自定义媒体查询\n`
	str += `\t * 可复写规则，以下为默认配置，如 md@bg-red diy@bg-red\n`
	str += `\t * sm           : '(min-width: 640px)',\n`
	str += `\t * md           : '(min-width: 768px)',\n`
	str += `\t * lg           : '(min-width: 1024px)',\n`
	str += `\t * xl           : '(min-width: 1280px)'\n`
	str += `\t */\n`
	str += `\tmediaQueries    : ${pluginOptions?.mediaQueries ?? '{}'},\n`
	str += `\t/**\n`
	str += `\t * 是否为所有css 添加 important\n`
	str += `\t */\n`
	str += `\timportant       : ${pluginOptions?.important ?? false},\n\n`
	str += `\t/**\n`
	str += `\t * 可变单位 v 的转换方式\n`
	str += `\t * 例如 w-10 => 10 / 16 => 0.625rem\n`
	str += `\t */\n`
	str += `\t// vToAny          : {\n`
	str += `\t//     unit: 'rem',\n`
	str += `\t//     rootValue: 16, // 跟字体大小或基于参数返回根元素字体大小 1px => 1/16rem\n`
	str += `\t//     unitPrecision: 5, // 允许小数单位精度\n`
	str += `\t//     minPixelValue: 1 // 不会被转换的最小值\n`
	str += `\t// }\n`
	if (pluginOptions?.shortcuts) {
		str += `\t/**\n`
		str += `\t * 快捷方式\n`
		str += `\t */\n`
		str += `\tshortcuts: ${JSON.stringify(pluginOptions.shortcuts)},\n`
	}
	str += `\t/**\n`
	str += `\t * 是否开启调试模式，开启后将输出详细的日志信息\n`
	str += `\t */\n`
	str += `\tdebug           : ${pluginOptions?.debug ?? true},\n`
	str += `\t/**\n`
	str += `\t * 是否生成全局css文件，默认开启\n`
	str += `\t */\n`
	str += `\tgenerateGlobalCss : ${pluginOptions?.generateGlobalCss ?? true},\n`
	str += `}\n`
	
	return str;
}