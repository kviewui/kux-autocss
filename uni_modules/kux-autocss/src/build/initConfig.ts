import path from 'path';
// import { type UserConfig } from 'vite'
import {
	PACKAGE_VERSION,
	GIT_REPOSITORY_URL,
	AUTHOR,
	CONFIG_CONTENT,
	formatDate,
	date
} from './constant';
import { CSS_FILE_NAME } from '../constant/index'
import { changeConsoleColor, consolePrint } from '../tools/colorUtils';
import { PluginOptions } from 'types';
import { getConfig, readConfigFile, getFilePath } from '../config/index'
import { getRoot } from '../tools/utils';

const fs = require('fs');
const fsSync = fs.promises; // 使用 fs.promises 来获取异步方法

export const genInitConfig = async (config: any, options?: PluginOptions) => {
	// const globalConfig = readConfigFile(config, options) as any
	// const outputPath = path.resolve(getRoot(config), options?.root ?? '', options?.configFile ?? './autocss.config.js');
	const outputPath = getFilePath(options?.configFile ?? './autocss.config.js', config, options)
	const dirPath = path.dirname(outputPath)
	
	if (!fs.existsSync(dirPath)) {
		fsSync.mkdir(dirPath)
	}
	
	// 检查文件是否存在
	if (!fs.existsSync(outputPath)) {
		try {
			// 尝试写入文件
			await fsSync.writeFile(outputPath, CONFIG_CONTENT(options));
			// console.info('autocss config write complete');
			if (options?.debug) {
				consolePrint('autocss config write complete', 'success');
			}
		} catch (err) {
			if (err.code === 'EACCES') {
				// 如果没有写入权限，尝试修改文件权限
				try {
					await fsSync.chmod(outputPath, 0o666);
					// 修改权限后再次尝试写入
					await fsSync.writeFile(outputPath, CONFIG_CONTENT(options));
					// console.info('autocss config write complete');
					if (options?.debug) {
						consolePrint('autocss config write complete', 'success');
					}
				} catch (chmodErr) {
					throw chmodErr;
				}
			} else {
				throw err;
			}
		}
	} else {
		// console.info('autocss config already exists, skipping write');
		if (options?.debug) {
			consolePrint('autocss config already exists, skipping write', 'warning');
		}
	}
}

export const initCssDir = async (config: any, options?: PluginOptions) => {
	// const outputPath = path.resolve(getRoot(config), options?.root ?? '', options?.cssFile ?? CSS_FILE_NAME);
	const outputPath = getFilePath(options?.cssFile ?? CSS_FILE_NAME, config, options)
	const cssDirPath = path.dirname(outputPath)
	
	// 检查目录是否存在
	if (!fs.existsSync(cssDirPath) && options?.generateGlobalCss) {
		await fsSync.mkdir(cssDirPath, { recursive: true })
	}
}

export const initApp = async (config: any, options?: PluginOptions) => {
	// const appPath = path.resolve(getRoot(config), options?.root ?? '', process.env.UNI_APP_X ? 'App.uvue' : 'App.vue');
	const appPath = getFilePath(process.env.UNI_APP_X ? 'App.uvue' : 'App.vue', config, options);
	// 判断缓存目录是否存在
	// const cacheDirPath = path.resolve(getRoot(config), options?.root ?? '', '.autocss')
	const cacheDirPath = getFilePath('.autocss', config, options)
	
	if (!fs.existsSync(cacheDirPath)) {
		await fsSync.mkdir(cacheDirPath, { recursive: true })
	}
	
	const autoimportStyleJsonPath = path.resolve(cacheDirPath, 'autoimport-style.json')
	// 判断缓存文件是否存在，存在则读取内容
	let autoimportStyleJson = {}
	try {
		autoimportStyleJson = JSON.parse(await fsSync.readFile(autoimportStyleJsonPath, 'utf-8'))
	} catch (err) {
		// 如果文件不存在，则创建空文件
		await fsSync.writeFile(autoimportStyleJsonPath, '{}')
	}
	
	// 判断样式导入是否已存在
	const styleImport = autoimportStyleJson['content']
	
	// 读取 App.uvue 文件内容
	let appContent = await fsSync.readFile(appPath, 'utf-8');
	let appendContent = ''
	
	const globalConfig = readConfigFile(config, options)
	
	if (globalConfig) {
		// 向 App.uvue 文件中插入 style 标签，并引入全局样式
		appendContent += '\n<style>\n'
		appendContent += `\t/**\n`
		appendContent += `\t * 该样式文件导入由autocss自动生成\n`
		appendContent += `\t * @version ${PACKAGE_VERSION}\n`
		appendContent += `\t * @author ${AUTHOR}\n`
		appendContent += `\t * @repository ${GIT_REPOSITORY_URL}\n`
		appendContent += `\t * @date ${formatDate(date)}\n`
		appendContent += `\t */\n`
		appendContent += '\t/* #ifdef WEB */\n'
		appendContent += `\t@import url(\'@/${globalConfig['cssFile']}\');\n`
		appendContent += '\t/* #endif */\n'
		appendContent += '</style>'
		
		// 如果样式导入已存在，则把 appContent 内容替换为新的内容
		if (styleImport) {
			appContent = appContent.replace(styleImport, appendContent)
		} else {
			appContent += appendContent
		}
		
		// 写入 App.uvue 文件内容
		await fsSync.writeFile(appPath, appContent);
		
		// 更新缓存文件
		autoimportStyleJson['content'] = appendContent
		await fsSync.writeFile(autoimportStyleJsonPath, JSON.stringify(autoimportStyleJson))
	}
}