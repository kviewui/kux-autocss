/**
 * 配置全局变量
 */
import { getColorsKey } from '../tools/colorUtils';
import { version } from '../../package.json';
import { AUTHOR, GIT_REPOSITORY_URL, NOW_DATE } from '../build/constant';

export const toRegexStr = (_: any[]) => _.join('|');
export const VERSION = version;
export const COLORS = 'colors';
export const ROOT = 'root';
export const GENERATE = 'generate';
export const UNIT = 'unit';
export const IMPORTANT = 'important';
export const GLOB_REG = 'globReg';
export const V_TO_ANY = 'vToAny';
export const BEFORE_CONTENTS = 'beforeContent';
export const AFTER_CONTENT = 'afterContent';
export const EXT_NAME = 'extName';
export const OVERRIDE_RULES = 'overrideRules';
export const MEDIA_QUERIES = 'mediaQueries';
export const CONFIG_FILE_NAME = 'autocss.config.js';
export const CONFIG_FILE = 'auto.config.ts';
export const CSS_FILE_NAME = 'style/auto.css';
export const CSS_FILE = 'cssFile';
export const ROOT_DIR = './';
export const PREFIX = 'prefix';
export const PREFIX_SEPARATOR = 'prefixSeparator';
export const AUTO_USE_SNIPPETS = 'autoUseSnippets';
export const SNIPPETS_FILE_NAME = 'autocss.snippets.css';
export const CSS_SNIPPETS_FILE = 'cssSnippetsFile'
// export const COLORS_NAME_PRESET = ['red', 'orangered', 'orange', 'gold', 'yellow', 'lime', 'green', 'cyan', 'blue', 'deepblue', 'purple', 'pinkpurple', 'magenta', 'grey'];
export const COLORS_NAME_PRESET = getColorsKey()

/**
 * 配置全局枚举
 */

/**
 * 对应 `ucss` 中的 `display` 属性，[参考 display](https://doc.dcloud.net.cn/uni-app-x/css/display.html)
 */
export const DISPLAY_ENUM = ['flex', 'none', 'hidden'];

/**
 * 对应 `ucss` 中的 `justify-content` 属性，[参考 justify-content](https://doc.dcloud.net.cn/uni-app-x/css/justify-content.html)
 */
export const JUSTIFY_CONTENT_ENUM = ['center', 'flex-start', 'flex-end', 'space-between', 'space-around', 'between', 'start', 'end', 'around'];

/**
 * 对应 `ucss` 中的 `align-items` 属性，[参考 align-items](https://doc.dcloud.net.cn/uni-app-x/css/align-items.html)
 */
export const ALIGN_ITEMS_ENUM = ['center', 'flex-start', 'flex-end', 'stretch', 'baseline', 'start', 'end'];

/**
 * 对应 `ucss` 中的 `长度单位`，[参考 长度单位](https://doc.dcloud.net.cn/uni-app-x/css/common/length.html)
 * + 支持百分比
 */
export const UNIT_ENUM = ['fr', 'px', 'rpx', 'upx', 'em', 'rem', 'cap', 'ch', 'cm', 'cqb', 'cqh', 'cqi', 'cqmax', 'cqmin', 'cqw', 'dvb', 'dvh', 'dvi', 'dvw', 'ex', 'ic', 'in', 'lh', 'lvb', 'lvh', 'lvi', 'lvw', 'mm', 'pc', 'pt', 'q', 'rcap', 'rch', 'rex', 'ric', 'rlh', 'svb', 'svh', 'svi', 'svw', 'vb', 'vh', 'vi', 'vmax', 'vmin', 'vw', 'p', 'v'];

/**
 * 对应 `css` 中的 `cursor` 属性，[参考 cursor](https://developer.mozilla.org/zh-CN/docs/Web/CSS/cursor)
 * + 只有web支持
 */
export const CURSOR_ENUM = ['auto', 'default', 'none', 'context-menu', 'help', 'pointer', 'progress', 'wait', 'cell', 'crossshair', 'text', 'vertical-text', 'alias', 'copy', 'move', 'no-drop', 'not-allowed', 'grab', 'grabbing', 'all-scroll', 'col-resize', 'row-resize', 'n-resize', 'e-resize', 's-resize', 'w-resize', 'ne-resize', 'nw-resize', 'se-resize', 'sw-resize', 'ew-resize', 'ns-resize', 'nesw-resize', 'nwse-resize', 'zoom-in', 'zoom-out'];

/**
 * 对应 `css` 中的 `gap` 属性，[参考 gap](https://developer.mozilla.org/zh-CN/docs/Web/CSS/gap)
 * + 只有web支持
 */
export const GAP_ENUM = ['inherit', 'initial', 'revert', 'revert-layer', 'unset'];

/**
 * 对应 `css` 中的 `vertical-align` 属性，[参考 vertical-align](https://developer.mozilla.org/zh-CN/docs/Web/CSS/vertical-align)
 * + 只有web支持
 */
export const VERTICAL_ALIGN_ENUM = ['baseline', 'sub', 'super', 'text-top', 'text-bottom', 'middle', 'top', 'bottom', 'inherit', 'initial', 'unset'];

const PSEUDO_ENUM = ['active', 'any-link', 'blank', 'checked', 'current', 'default', 'defined', 'disabled', 'drop', 'empty', 'enabled', 'first', 'first-child', 'first-of-type', 'fullscreen', 'future', 'focus', 'focus-visible', 'focus-within', 'host', 'hover', 'indeterminate', 'in-range', 'invalid', 'last-child', 'last-of-type', 'left', 'link', 'local-link', 'only-child', 'only-of-type', 'optional', 'out-of-range', 'past', 'placeholder-shown', 'read-only', 'read-write', 'required', 'right', 'root', 'scope', 'target', 'target-within', 'user-invalid', 'valid', 'visited'];

/**
 * flex-direction 枚举
 * @see [ucss|flex-direction](https://doc.dcloud.net.cn/uni-app-x/css/flex-direction.html)
 */
export const FLEX_DIRECTION_NAME_ENUM = ['flex-direction', 'flex'];
export const FLEX_DIRECTION_ENUM = ['row', 'row-reverse', 'column', 'col', 'column-reverse', 'col-reverse'];
export const FLEX_DIRECTION_NAME_STR = toRegexStr(FLEX_DIRECTION_NAME_ENUM);
export const FLEX_DIRECTION_STR = toRegexStr(FLEX_DIRECTION_ENUM);

/**
 * text-align 枚举
 * @see [ucss|text-align](https://doc.dcloud.net.cn/uni-app-x/css/text-align.html)
 */
export const TEXT_ALIGN_NAME_ENUM = ['text-align', 'text'];
export const TEXT_ALIGN_ENUM = ['left', 'right', 'center', 'justify', 'start', 'end', 'match-parent'];
export const TEXT_ALIGN_NAME_STR = toRegexStr(TEXT_ALIGN_NAME_ENUM);
export const TEXT_ALIGN_STR = toRegexStr(TEXT_ALIGN_ENUM);

/**
 * align-content 枚举
 * @see [ucss|align-content](https://doc.dcloud.net.cn/uni-app-x/css/align-content.html)
 */
export const ALIGN_CONTENT_NAME_ENUM = ['align-content', 'content']
export const ALIGN_CONTENT_ENUM = ['start', 'end', 'center', 'between', 'around', 'stretch', 'normal']
export const ALIGN_CONTENT_NAME_STR = toRegexStr(ALIGN_CONTENT_NAME_ENUM)
export const ALIGN_CONTENT_STR = toRegexStr(ALIGN_CONTENT_ENUM)

/**
 * align-self 枚举
 * @see [ucss|align-self](https://doc.dcloud.net.cn/uni-app-x/css/align-self.html)
 */
export const ALIGN_SELF_NAME_ENUM = ['align-self', 'self']
export const ALIGN_SELF_ENUM = ['auto', 'start', 'end', 'center', 'baseline', 'stretch']
export const ALIGN_SELF_NAME_STR = toRegexStr(ALIGN_SELF_NAME_ENUM)
export const ALIGN_SELF_STR = toRegexStr(ALIGN_SELF_ENUM)

/**
 * background-clip 枚举
 * @see [ucss|background-clip](https://doc.dcloud.net.cn/uni-app-x/css/background-clip.html)
 */
export const BACKGROUND_CLIP_NAME_ENUM = ['bg-clip']
export const BACKGROUND_CLIP_ENUM = ['border', 'padding', 'content']
export const BACKGROUND_CLIP_NAME_STR = toRegexStr(BACKGROUND_CLIP_NAME_ENUM)
export const BACKGROUND_CLIP_STR = toRegexStr(BACKGROUND_CLIP_ENUM)

/**
 * box-shadow size 枚举
 * @see [ucss|box-shadow](https://doc.dcloud.net.cn/uni-app-x/css/box-shadow.html)
 */
export const BOX_SHADOW_SIZE_ENUM = ['sm', '', 'md', 'lg', 'xl', '2xl', 'inner', 'none'];
export const BOX_SHADOW_SIZE_STR = toRegexStr(BOX_SHADOW_SIZE_ENUM);
export const BOX_SHADOW_SIZE_MAP = new Map()
BOX_SHADOW_SIZE_MAP.set('sm', '0 1px 2px 0 rgb(0 0 0 / 0.05)');
BOX_SHADOW_SIZE_MAP.set('', '0 1px 3px 0 rgb(0 0 0 / 0.1), 0 1px 2px -1px rgb(0 0 0 / 0.1)');
BOX_SHADOW_SIZE_MAP.set('md', '0 4px 6px -1px rgb(0 0 0 / 0.1), 0 2px 4px -2px rgb(0 0 0 / 0.1)');
BOX_SHADOW_SIZE_MAP.set('lg', '0 10px 15px -3px rgb(0 0 0 / 0.1), 0 4px 6px -4px rgb(0 0 0 / 0.1)');
BOX_SHADOW_SIZE_MAP.set('xl', '0 20px 25px -5px rgb(0 0 0 / 0.1), 0 8px 10px -6px rgb(0 0 0 / 0.1)');
BOX_SHADOW_SIZE_MAP.set('2xl', '0 25px 50px -12px rgb(0 0 0 / 0.25)');
BOX_SHADOW_SIZE_MAP.set('inner', 'inset 0 2px 4px 0 rgb(0 0 0 / 0.05)');
BOX_SHADOW_SIZE_MAP.set('none', '0 0 #0000');

/**
 * flex-flow 枚举
 * @see [ucss|flex-flow](https://doc.dcloud.net.cn/uni-app-x/css/flex-flow.html)
 */
export const FLEX_FLOW_NAME_ENUM = ['flex-flow', 'flow', 'ff']
export const FLEX_FLOW_DIRECTION_ENUM = ['row', 'row-reverse', 'column', 'col', 'column-reverse', 'col-reverse']
export const FLEX_FLOW_WRAP_ENUM = ['nowrap', 'wrap', 'wrap-reverse']
export const FLEX_FLOW_NAME_STR = toRegexStr(FLEX_FLOW_NAME_ENUM)
export const FLEX_FLOW_DIRECTION_STR = toRegexStr(FLEX_FLOW_DIRECTION_ENUM)
export const FLEX_FLOW_WRAP_STR = toRegexStr(FLEX_FLOW_WRAP_ENUM)

/**
 * text-decoration-style 枚举
 * @see [ucss|text-decoration-style](doc.dcloud.net.cn/uni-app-x/css/text-decoration-style.html)
 */
export const TEXT_DECORATION_STYLE_VALUE_ENUM = ['solid', 'double', 'dotted', 'dashed', 'wavy']
export const TEXT_DECORATION_STYLE_VALUE_STR = toRegexStr(TEXT_DECORATION_STYLE_VALUE_ENUM)

/**
 * transition-property 枚举
 * @see [ucss|transition-property](https://doc.dcloud.net.cn/uni-app-x/css/transition-property.html)
 */
export const TRANSITION_PROPERTY_VALUE_ENUM = ['all', 'none', 'width', 'height', 'margin', 'margin-top', 'margin-bottom', 'margin-left', 'margin-right', 'left', 'right', 'top', 'bottom', 'orientation', 'padding', 'padding-left', 'padding-right', 'padding-top', 'padding-bottom', 'opacity', 'background-color', 'border-color', 'border-top-color', 'border-bottom-color', 'border-left-color', 'border-right-color', 'colors', 'transform']
export const TRANSITION_PROPERTY_VALUE_STR = toRegexStr(TRANSITION_PROPERTY_VALUE_ENUM)

/**
 * transition-timing-function 枚举
 * @see [ucss|transition-timing-function](https://doc.dcloud.net.cn/uni-app-x/css/transition-timing-function.html)
 */
export const TRANSITION_TIMING_FUNCTION_VALUE_ENUM = ['linear', 'in', 'out', 'in-out']
export const TRANSITION_TIMING_FUNCTION_VALUE_STR = toRegexStr(TRANSITION_TIMING_FUNCTION_VALUE_ENUM)

/**
 * text-overflow 枚举
 * @see [ucss|text-overflow](https://doc.dcloud.net.cn/uni-app-x/css/text-overflow.html)
 */
export const TEXT_OVERFLOW_VALUE_ENUM = ['clip', 'ellipsis']
export const TEXT_OVERFLOW_VALUE_STR = toRegexStr(TEXT_OVERFLOW_VALUE_ENUM)

/**
 * white-space 枚举
 * @see [ucss|white-space](https://doc.dcloud.net.cn/uni-app-x/css/white-space.html)
 */
export const WHITE_SPACE_VALUE_ENUM = ['normal', 'nowrap', 'pre', 'pre-wrap', 'pre-line', 'break-spaces']
export const WHITE_SPACE_VALUE_STR = toRegexStr(WHITE_SPACE_VALUE_ENUM)


export const DISPLAY_STR = toRegexStr(DISPLAY_ENUM);
export const PSEUDO_STR = toRegexStr(PSEUDO_ENUM);
export const JUSTIFY_CONTENT_STR = toRegexStr(JUSTIFY_CONTENT_ENUM);
export const ALIGN_ITEMS_STR = toRegexStr(ALIGN_ITEMS_ENUM);
export const CURSOR_STR = toRegexStr(CURSOR_ENUM);
export const UNIT_STR = toRegexStr(UNIT_ENUM);
export const VERTICAL_ALIGN_STR = toRegexStr(VERTICAL_ALIGN_ENUM);
export const GAP_STR = toRegexStr(GAP_ENUM);

/**
 * 输出的css文件头部内容
 */ 
export let CSS_ANNOTATION = '';

CSS_ANNOTATION += '/* \n';
CSS_ANNOTATION += `  @see ${GIT_REPOSITORY_URL}\n`;
CSS_ANNOTATION += `  @version: v${version}\n`;
CSS_ANNOTATION += `  @author ${AUTHOR}\n`;
CSS_ANNOTATION += `  ${NOW_DATE}\n`;
CSS_ANNOTATION += '  该文件将会自动初始化并更新，切勿直接修改！\n';
CSS_ANNOTATION += '*/\n\n';

/**
 * 媒体查询
 */
export const BASE_MEDIA_QUERY = {
    sm: '(min-width: 640px)',
    md: '(min-width: 768px)',
    lg: '(min-width: 1024px)',
    xl: '(min-width: 1280px)'
};

export const BASE_MEDIA_QUERY_KEY = Object.keys(BASE_MEDIA_QUERY);

/**
 * 定义class中的数字变体正则
 */
// export const NONNEGATIVE_NUMBER_REGEX_STR = '(0|([1-9]\\d*))(\\.\\d*[1-9])?';
export const NONNEGATIVE_NUMBER_REGEX_STR = '(0|([1-9]\\d*))(\\.\\d*[1-9])?(?:\\/([1-9]\\d*))?';

/**
 * 定义class中的方向基础变体映射关系
 */
export const DIRECTION_MAP = new Map();
DIRECTION_MAP.set(undefined, ['']);
DIRECTION_MAP.set('x', ['left', 'right']);
DIRECTION_MAP.set('y', ['top', 'bottom']);
DIRECTION_MAP.set('t', ['top']);
DIRECTION_MAP.set('b', ['bottom']);
DIRECTION_MAP.set('l', ['left']);
DIRECTION_MAP.set('r', ['right']);
DIRECTION_MAP.set('top', ['top']);
DIRECTION_MAP.set('bottom', ['bottom']);
DIRECTION_MAP.set('left', ['left']);
DIRECTION_MAP.set('right', ['right']);

/**
 * 定义圆角类型class中的方向基础变体映射关系
 */
export const RADIUS_DIRECTION_MAP = new Map();
RADIUS_DIRECTION_MAP.set(undefined, ['']);
RADIUS_DIRECTION_MAP.set('t', ['top-left', 'top-right'])
RADIUS_DIRECTION_MAP.set('b', ['bottom-left', 'bottom-right'])
RADIUS_DIRECTION_MAP.set('l', ['top-left', 'bottom-left'])
RADIUS_DIRECTION_MAP.set('r', ['top-right', 'bottom-right'])
RADIUS_DIRECTION_MAP.set('tl', ['top-left'])
RADIUS_DIRECTION_MAP.set('tr', ['top-right'])
RADIUS_DIRECTION_MAP.set('bl', ['bottom-left'])
RADIUS_DIRECTION_MAP.set('br', ['bottom-right'])
RADIUS_DIRECTION_MAP.set('top', ['top-left', 'top-right'])
RADIUS_DIRECTION_MAP.set('bottom', ['bottom-left', 'bottom-right'])
RADIUS_DIRECTION_MAP.set('left', ['top-left', 'bottom-left'])
RADIUS_DIRECTION_MAP.set('right', ['top-right', 'bottom-right'])

/**
 * 定义渐变色类型class中的方向基础变体映射关系
 */
export const GRADIENT_DIRECTION_MAP = new Map();
GRADIENT_DIRECTION_MAP.set('t', 'top');
GRADIENT_DIRECTION_MAP.set('b', 'bottom');
GRADIENT_DIRECTION_MAP.set('l', 'left');
GRADIENT_DIRECTION_MAP.set('r', 'right');

/**
 * 定义缩放类型class中的方向基础变体映射关系
 */
export const SCALE_DIRECTION_MAP = new Map();
SCALE_DIRECTION_MAP.set(undefined, 'scale');
SCALE_DIRECTION_MAP.set('x', 'scaleX');
SCALE_DIRECTION_MAP.set('y', 'scaleY');

/**
 * 定义旋转类型class中的方向基础变体映射关系
 */
export const ROTATE_DIRECTION_MAP = new Map();
ROTATE_DIRECTION_MAP.set(undefined, 'rotate');
ROTATE_DIRECTION_MAP.set('x', 'rotateX');
ROTATE_DIRECTION_MAP.set('y', 'rotateY');
ROTATE_DIRECTION_MAP.set('z', 'rotateZ');

/**
 * 定义位移类型class中的方向基础变体映射关系
 */
export const TRANSLATE_DIRECTION_MAP = new Map();
TRANSLATE_DIRECTION_MAP.set(undefined, 'translate');
TRANSLATE_DIRECTION_MAP.set('x', 'translateX');
TRANSLATE_DIRECTION_MAP.set('y', 'translateY');

/**
 * 定义变形原点类型class中的方向基础变体映射关系
 */
export const TRANSFORM_ORIGIN_DIRECTION_MAP = new Map();
TRANSFORM_ORIGIN_DIRECTION_MAP.set('b', 'bottom')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('bottom', 'bottom')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('l', 'left')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('left', 'left')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('r', 'right')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('right', 'right')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('t', 'top')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('top', 'top')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('c', 'center')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('center', 'center')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('bc', 'bottom center')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('bottom-center', 'bottom center')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('bl', 'bottom left')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('bottom-left', 'bottom left')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('br', 'bottom right')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('bottom-right', 'bottom right')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('cl', 'center left')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('center-left', 'center left')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('cr', 'center right')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('center-right', 'center right')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('cb', 'center bottom')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('center-bottom', 'center bottom')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('cc', 'center center')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('center-center', 'center center')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('ct', 'center top')
TRANSFORM_ORIGIN_DIRECTION_MAP.set('center-top', 'center top')




