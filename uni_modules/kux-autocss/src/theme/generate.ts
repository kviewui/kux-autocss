import { GenerateColors, Colors, DynamicStringObj } from '../types'
import { getConfig } from '../config'

export function generateColors(colors?: Colors, parentKey = ''): GenerateColors[] {
	let generateColorsRes: GenerateColors[] = []
	const theme = getConfig('theme')
	colors = colors || (theme ? theme.colors : {})
	for (const key in colors) {
		const color = colors[key]
		const className = parentKey? `${parentKey}-${key}` : key
		if (typeof color === 'string') {
			// 如果是字符串，则直接添加到生成颜色数组中
			const generateColor: GenerateColors = {
				className: className,
				value: color
			}
			generateColorsRes.push(generateColor)
		} else if (typeof color === 'object' && color !== null) {
			// 如果是对象，则递归处理，并展开结果数组
			generateColorsRes = generateColorsRes.concat(generateColors(color, className))
		}
	}
	
	return generateColorsRes
}