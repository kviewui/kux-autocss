import { defineConfig } from 'vite';
import uni from '@dcloudio/vite-plugin-uni';
import autocss from './uni_modules/kux-autocss';

export default defineConfig({
	plugins: [
		uni(),
		autocss({
			configFile: 'autocss/config.js',
			cssFile: 'autocss/index.css',
			include: ['pages/**', 'components/**', 'autocss/**'],
			autoUseSnippets: true,
			cssSnippetsFile: 'autocss/snippets.css',
			shortcuts: {
				'kux-btn': 'py-2 px-4 font-semibold rounded-lg shadow-md'
			},
			theme: {
				colors: {
					// primary: '#3491FA',
					primary: {
						0: '#E8F7FF'
					}
				}
			}
		})
	]
});